#ifndef __SR_DEFS__
#define __SR_DEFS__

#ifdef __cplusplus
extern "C" { 
#endif // !__cplusplus

#if defined(_WIN32) && !defined(_WIN64)
typedef long long          i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#elif defined(_WIN64)
typedef __int64            i64;
typedef __int32            i32;
typedef __int16            i16;
typedef __int8              i8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef long int           i64;
typedef int                i32;
typedef short              i16;
typedef char                i8;
#elif defined(__APPLE__) && defined(__MACH__)
typedef unsigned long int  i64;
typedef unsigned int       i32;
typedef unsigned short     i16;
typedef unsigned char       i8;
#endif

#ifdef _WIN32
typedef signed long long   s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#elif defined(_WIN64)
typedef signed __int64     s64;
typedef signed __int32     s32;
typedef signed __int16     s16;
typedef signed __int8       s8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef signed long int    s64;
typedef signed int         s32;
typedef signed short       s16;
typedef signed char         s8;
#elif defined(__APPLE__) && defined(__MACH__)
typedef unsigned long int  s64;
typedef unsigned int       s32;
typedef unsigned short     s16;
typedef unsigned char       s8;
#endif

#if defined(_WIN32)
typedef unsigned long long u64;
typedef unsigned int       u32;
typedef unsigned short     u16;
typedef unsigned char       u8;
#elif defined(_WIN64)
typedef unsigned __int64   u64;
typedef unsigned __int32   u32;
typedef unsigned __int16   u16;
typedef unsigned __int8     u8;
#elif defined(__linux__) || defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
typedef unsigned long int  u64;
typedef unsigned int       u32;
typedef unsigned short     u16;
typedef unsigned char       u8;
#elif defined(__APPLE__) && defined(__MACH__)
typedef unsigned long int  u64;
typedef unsigned int       u32;
typedef unsigned short     u16;
typedef unsigned char       u8;
#endif

#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64) || defined(_M_X64) || defined(_M_AMD64)
#define usizemax 0xffffffffffffffffui64
typedef i64 imax;
typedef u64 umax;
typedef u64 usize;
typedef i64 isize;
typedef i64 ptrdiff;
typedef i64 iptr;
typedef u64 uptr;
#elif defined(i386) || defined(__i386) || defined(__i386__) || defined(__IA32__) || defined(_M_IX86) || defined(__x86__) || defined(_X86_)
#define usizemax 0xffffffffui32
typedef i32      imax;
typedef u32      umax;
typedef u32      usize;
typedef i32      isize;
typedef i32      ptrdiff;
typedef i32      iptr;
typedef u32      uptr;
#endif 

typedef float       f32; 
typedef long double f64;    

typedef i32 b32;
typedef i16 b16;
typedef i8  b8;

#define i8min        (-127i8 - 1)
#define i16min       (-32767i16 - 1)
#define i32min       (-2147483647i32 - 1)
#define i64min       (-9223372036854775807i64 - 1)
#define i8max        127i8
#define i16max       32767i16
#define i32max       2147483647i32
#define i64max       9223372036854775807i64
#define u8max        0xffui8
#define u16max       0xffffui16
#define u32max       0xffffffffui32
#define u64max       0xffffffffffffffffui64

#if !defined(__cplusplus)
#define true  1
#define false 0
#endif

#if defined(__GNUC__) || defined(__clang__)
#define SR_FORCEINLINE inline __attribute__((always_inline))
#elif defined(_MSC_VER)
#define SR_FORCEINLINE __forceinline
#else
#define SR_FORCEINLINE inline
#endif


#define MIN(a,b) ( (a) < (b) ? (a) : (b))
#define MAX(a,b) ( (a) > (b) ? (a) : (b))

#define UNUSED_VARIABLE(v) ( ( void ) v )
#define ARRAY_COUNT( a )     ( sizeof ( a ) / sizeof ( ( a ) [ 0 ] ) )

#define ALIGN_DOWN(x,a)      ( ( x ) & ~( ( a ) - 1 ) )
#define ALIGN_UP(x,a)        ALIGN_DOWN ( ( x ) + ( a ) - 1, ( a ) )
#define ALIGN_DOWN_PTR(p, a) ((void *)ALIGN_DOWN((uptr)(p), (a)))
#define ALIGN_UP_PTR(p, a)   ((void *)ALIGN_UP((uptr)(p), (a)))

#define SWAP16 ( ( ( ( x ) >> 8 ) & 0x00FF ) | ( ( ( x ) << 8 ) & 0xFF00 ) )

#define SWAP32(x) \
 ( ( ( ( x ) >> 24 ) & 0x000000FF ) | ( ( ( x ) >> 8  ) & 0x0000FF00 ) | \
   ( ( ( x ) <<  8 ) & 0x00FF0000 ) | ( ( ( x ) << 24 ) & 0xFF000000 ) )

#define SWAP64(x) \
  ( ( ( ( x ) >> 56 ) & 0x00000000000000FF ) | ( ( ( x ) >> 40 ) & 0x000000000000FF00 ) | \
	( ( ( x ) >> 24 ) & 0x0000000000FF0000 ) | ( ( ( x ) >> 8  ) & 0x00000000FF000000 ) | \
	( ( ( x ) << 8  ) & 0x000000FF00000000 ) | ( ( ( x ) << 24 ) & 0x0000FF0000000000 ) | \
	( ( ( x ) << 40 ) & 0x00FF000000000000 ) | ( ( ( x ) << 56 ) & 0xFF00000000000000 ) )

#define KB(x) (      x   * 1024 )
#define MB(x) ( KB ( x ) * 1024 )
#define GB(x) ( MB ( x ) * 1024 )
#define TB(x) ( GB ( x ) * 1024 )
#define PB(x) ( TB ( x ) * 1024 )


#ifdef __cplusplus
}
#endif // !__cplusplus



#endif // !__SR_DEFS__